package services

import models._
import play.api.libs.json.{JsObject, JsSuccess, JsValue, Json}
import play.api.libs.ws.{WSAuthScheme, WSClient}
import play.api.libs.ws.JsonBodyWritables._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class ClusterstatsService{

  /**
    * Retrieve all of the services running in the swarm
    * @param ws
    * @param uri
    * @return
    */
  def getServices(ws: WSClient, uri: String) : Future[Map[String, DeployedTool]] = {
    val (url, username, password) = ClusterConfig.parseURI(uri)
    val foo = ws.url(url + "/services?full=1").withAuth(username, password, WSAuthScheme.BASIC).get()
    foo.map(r => {
      r.status match {
        case 200 => parseToMap(r.json.as[JsObject])
        case _ => Map[String, DeployedTool]()
      }
    })
  }

  /**
    * Retrieve just the extractors running in the swarm
    * @param ws
    * @param uri
    * @return
    */
  def getExtractors(ws: WSClient, uri: String): Future[Iterable[DeployedTool]] = {
    val serviceMapFut = getServices(ws, uri)

    for {
      serviceMap <- serviceMapFut
      serviceList = serviceMap.values }
      yield {
        for {
          serviceObj <- serviceList
          service = serviceObj.service
          labels <- service.labels if labels.bdType == "extractor"
        }yield {
          serviceObj
        }
      }
  }


  /**
    * Filter the list of extractors running in the swarm to just those with the indicated vhost
    * @param ws
    * @param uri
    * @param vhost
    * @return
    */
  def getExtractorsForVhost(ws: WSClient, uri: String, vhost:String): Future[Iterable[DeployedTool]] = {
    getExtractors(ws, uri).map(extractors => {
      extractors.filter(extractor => extractor.service.labels.getOrElse(Label.dummyLabel).vhost == vhost)
    })
  }

  /**
    * Create a map of serviceId -> deployedTool
    * The object returned by clusterstats is dictionary of keys and deployed tool objects. This parses it
    * cleanly and generates a scala-friendly map
    * @param jsonObject
    * @return
    */
  def parseToMap(jsonObject : JsObject) : Map[String, DeployedTool] = {
    (for {
      serviceId: String <- jsonObject.keys
      json: JsValue = jsonObject.value(serviceId)
      if {json.validate[Service]
      match {
        case JsSuccess(_, _) => true
        case _ => false
      }
      }
    } yield (serviceId -> DeployedTool(serviceId, json.as[Service]))) (collection.breakOut)
  }

  /**
    * Deploy a new service to the swarm
    * @param ws
    * @param uri
    * @param deployParams
    * @return
    */
  def deploy(ws: WSClient, uri: String, deployParams : DeployParams) : Future[Try[String]] = {
    val (url, username, password) = ClusterConfig.parseURI(uri)
    val resultFut = ws.url(url + "/services").withAuth(username, password, WSAuthScheme.BASIC).post(Json.toJson(deployParams))

    resultFut.map(response => {
      response.status match {
        case 200 => Success(response.body)
        case _ => Failure(new Exception(response.body))
      }
    })
  }
}
