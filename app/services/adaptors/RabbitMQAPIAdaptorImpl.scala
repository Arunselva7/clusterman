package services.adaptors

import com.rabbitmq.http.client.Client

class RabbitMQAPIAdaptorImpl extends RabbitMQAPIAdaptor {
  def createClient(url:String, username:String, password:String): Client = {
    new Client(url, username, password)
  }
}
