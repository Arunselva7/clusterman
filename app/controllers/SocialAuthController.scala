package controllers

import javax.inject.Inject
import org.webjars.play.WebJarsUtil
import services.UserService
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.impl.providers._
import models.ClusterConfigs
import play.api.i18n.{I18nSupport, Messages}
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import play.api.data._
import play.api.data.Forms._
import utils.auth.{ DefaultEnv, LDAPInfo }

import scala.concurrent.{ExecutionContext, Future}

/**
  * The social auth controller.
  *
  * @param components             The Play controller components.
  * @param silhouette             The Silhouette stack.
  * @param userService            The user service implementation.
  * @param authInfoRepository     The auth info service implementation.
  * @param socialProviderRegistry The social provider registry.
  * @param ex                     The execution context.
  */
class SocialAuthController @Inject() (
                                       components: ControllerComponents,
                                       silhouette: Silhouette[DefaultEnv],
                                       userService: UserService,
                                       authInfoRepository: AuthInfoRepository,
                                       socialProviderRegistry: SocialProviderRegistry,
                                       clusterConfigs: ClusterConfigs
                                     )(
                                       implicit
                                       webJarsUtil: WebJarsUtil,
                                       assets: AssetsFinder,
                                       ex: ExecutionContext
                                     ) extends AbstractController(components) with I18nSupport with Logger {

  implicit val clusters: ClusterConfigs = clusterConfigs
  val loginForm = Form(
    mapping(
      "username" -> text,
      "password" -> text,
      "code" -> text
    )(LDAPInfo.apply)(LDAPInfo.unapply)
  )

  /**
    * Authenticates a user against a social provider.
    *
    * @param provider The ID of the provider to authenticate against.
    * @return The result to display.
    */
  def authenticate(provider: String) = Action.async { implicit request: Request[AnyContent] =>
    (socialProviderRegistry.get[SocialProvider](provider) match {
      case Some(p: SocialProvider with CommonSocialProfileBuilder) =>
        p.authenticate().flatMap {
          case Left(result) =>  Future.successful(result)
          case Right(authInfo) => {
            for {
              profile <- p.retrieveProfile(authInfo)
              user <- userService.save(profile)
              authInfo <- authInfoRepository.save(profile.loginInfo, authInfo)
              authenticator <- silhouette.env.authenticatorService.create(profile.loginInfo)
              value <- silhouette.env.authenticatorService.init(authenticator)
              result <- silhouette.env.authenticatorService.embed(value, Redirect(routes.ApplicationController.index()))
            } yield {
              silhouette.env.eventBus.publish(LoginEvent(user, request))
              result
            }
          }
        }
      case s => {
        // process for login with ldap:
        // 1. GET /authenticate/LDAP, same as other providers
        // 2. GET /authenticate/ldapinput, this case, to a page for input username & password. code as a hidden field
        // 3. POST /authenticate/LDAP, ldapauthenticate
        if(provider == "ldapinput"){
          Future.successful(Ok(views.html.login(loginForm)))
        } else {
          Future.failed(new ProviderException(s"Cannot authenticate with unexpected social provider $provider"))
        }
      }
    }).recover {
      case e: ProviderException =>
        logger.error("Unexpected provider error", e)
        Redirect(routes.SignInController.view()).flashing("error" -> Messages("could.not.authenticate"))
    }
  }

  def ldapauthenticate() = Action.async { implicit request =>
    loginForm.bindFromRequest.fold (
      errors => {
        Future(BadRequest("Invalid input data"))
      },
      authInfo2 => {
        (socialProviderRegistry.get[SocialProvider]("LDAP") match {
          case Some(p: SocialProvider with CommonSocialProfileBuilder) =>
            // TODO: refactory this duplicate code.
            p.authenticate().flatMap {
              case Left(result) => {
                Future.successful(result)
              }
              case Right(authInfo) => {
                // TODO: use authInfo2, no need to go through the whole process.
                for {
                  profile <- p.retrieveProfile(authInfo)
                  user <- userService.save(profile)
                  authInfo <- authInfoRepository.save(profile.loginInfo, authInfo)
                  authenticator <- silhouette.env.authenticatorService.create(profile.loginInfo)
                  value <- silhouette.env.authenticatorService.init(authenticator)
                  result <- silhouette.env.authenticatorService.embed(value, Redirect(routes.ApplicationController.index()))
                } yield {
                  silhouette.env.eventBus.publish(LoginEvent(user, request))
                  result
                }
              }
            }
          case _ => Future.failed(new ProviderException("Cannot authenticate with unexpected social provider LDAP"))
        }).recover {
          case e: ProviderException =>
            logger.error("Unexpected provider error", e)
            Redirect(routes.SocialAuthController.authenticate("ldapinput")).flashing("error" -> Messages("could.not.authenticate"))
        }
      }
    )
  }
  // TODO: refactory this duplicate code.
  //  private def authenticateHelper(p: SocialProvider ): Action.async ={
  //
  //  }

}
