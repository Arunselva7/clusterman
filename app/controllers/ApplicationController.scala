package controllers

import javax.inject.Inject

import com.mohiva.play.silhouette.api.actions.SecuredRequest
import com.mohiva.play.silhouette.api.{LogoutEvent, Silhouette}
import models.ClusterConfigs
import models.daos.ProjectDAO
import org.webjars.play.WebJarsUtil
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents}
import utils.auth.DefaultEnv

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * The basic application controller.
  *
  * @param components  The Play controller components.
  * @param silhouette  The Silhouette stack.
  * @param webJarsUtil The webjar util.
  * @param assets      The Play assets finder.
  */
class ApplicationController @Inject() (
                                        components: ControllerComponents,
                                        silhouette: Silhouette[DefaultEnv],
                                        config: Configuration,
                                        clusterConfigs: ClusterConfigs,
                                        projectDAO: ProjectDAO
                                      )(
                                        implicit
                                        webJarsUtil: WebJarsUtil,
                                        assets: AssetsFinder
                                      ) extends AbstractController(components) with I18nSupport {

  implicit val clusters: ClusterConfigs = clusterConfigs


  /**
    * Handles the index action.
    *
    * @return The result to display.
    */
  def index = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {

    // If we come in without a cluster selected, then go ahead and choose a default
    val clusterName = request.getQueryString("cluster") match {
      case Some(clusterName) => Some(clusterName)
      case _ => ClusterConfigs.defaultConfigName(clusterConfigs)
    }

    for (projects <- projectDAO.getProjectsForCluster(clusterName.getOrElse(""))) yield
      Ok(views.html.home(request.identity, projects, clusterName))
  }
  }

  /**
    * Handles the Sign Out action.
    *
    * @return The result to display.
    */
  def signOut = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] =>
    val result = Redirect(routes.ApplicationController.index())
    silhouette.env.eventBus.publish(LogoutEvent(request.identity, request))
    silhouette.env.authenticatorService.discard(request.authenticator, result)
  }


}