package models

import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}


object ToolType extends Enumeration {
  type ToolType = Value
  val Extractor, Converter = Value
}

import ToolType._

case class Project(
                    name: String,
                    toolType: ToolType,
                    cluster: String
                  ) {
}

object Project {

  implicit object ProjectReader extends BSONDocumentReader[Project] {
    def read(doc: BSONDocument): Project = {
      val name = doc.getAs[String]("name")
      val toolTypeName = doc.getAs[String]("toolType")
      val cluster = doc.getAs[String]("cluster")

      (name, toolTypeName, cluster) match {
        case (
          Some(name),
          Some(toolTypeName),
          Some(cluster)
          ) => Project(name, ToolType.withName(toolTypeName), cluster)
        case _ => throw new Exception("invalid project read from database")

      }
    }
  }

  implicit object ProjectWriter extends BSONDocumentWriter[Project] {
    def write(project: Project): BSONDocument = {
      BSONDocument(
        "name" -> project.name,
        "toolType" -> project.toolType.toString,
        "cluster" -> project.cluster
      )
    }
  }

}