package models

import java.net.URI

import play.api.Configuration

import scala.beans.BeanProperty
import com.typesafe.config.{ConfigBeanFactory, Optional}
import scala.collection.JavaConverters._



case class RabbitMQConfig(
                           @BeanProperty var toolType: String,
                           @BeanProperty var rabbitMQURI: String,
                           @BeanProperty var secret: String
                         ){

  //needed by configfactory to conform to java bean standard
  def this() = this("", "", "")

}

case class ClusterConfig(
                          @BeanProperty var name: String,
                          @BeanProperty var swarmURI: String, // URI in form of http://<userame>:<password>@host:port/path
                          @BeanProperty var vhost: String,
                          @BeanProperty var rabbitMQ: java.util.List[RabbitMQConfig]
                        ){
  def this() = this("", "", "", new java.util.ArrayList[RabbitMQConfig]())
}

case class ClusterConfigs(
      @BeanProperty var clusterConfigs: java.util.List[ClusterConfig]
                         ){
  def this() = this(new java.util.ArrayList[ClusterConfig]())
}

object ClusterConfigs{
  def loadFromConfig(configuration: Configuration): ClusterConfigs = {
    ConfigBeanFactory.create(configuration.underlying.getConfig("clusterman"), classOf[ClusterConfigs])
  }

  def getConfig(clusterConfigs: ClusterConfigs, aClusterName:String): Option[ClusterConfig] =
    clusterConfigs.clusterConfigs.asScala.find(cluster => cluster.name.equals(aClusterName))

  // Pick a cluster config to show as the default if no cluster is selected
  def defaultConfigName(clusterConfigs: ClusterConfigs): Option[String] = {
    if (clusterConfigs.clusterConfigs.size() > 0) Some(clusterConfigs.clusterConfigs.get(0).name) else None
  }
}

object ClusterConfig {

  /**
    * Parse a URI of the form
    * http://<username>:<password>@<host>:<port>/<path> into a tuple
    *
    * @param uriString
    * @throws java.lang.IllegalArgumentException
    * @return Tuple (URL String, username, password)
    */
  @throws(classOf[IllegalArgumentException])
  def parseURI(uriString: String): (String, String, String) = {
    val uri = URI.create(uriString)

    // Construct a URL without the user info
    val url = new URI(uri.getScheme, null, uri.getHost, uri.getPort, uri.getPath, null, null)

    val userInfo = uri.getUserInfo().split(":")
    if (userInfo.length != 2)
      throw new IllegalArgumentException

    (url.toString, userInfo(0), userInfo(1))
  }

  def getRabbitMQEntry(clusterConfig: ClusterConfig, aToolType:String): Option[RabbitMQConfig] =
    clusterConfig.rabbitMQ.asScala.find(rabbit => rabbit.toolType.toLowerCase.equals(aToolType.toLowerCase))
}


  object RabbitMQConfig{
    /**
      * Parse a RabbitMQ uri - for now we are just interested in the vhost
      * @param uriString
      * @return
      */
    def parseURI(uriString:String): String = {
      val uri = URI.create(uriString)
      uri.getRawPath.drop(1) // Path has a leading slash
    }
}
