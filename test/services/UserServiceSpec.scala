package services

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import models.User
import models.daos.UserDAO
import org.mockito.Mockito._
import play.api.test.PlaySpecification
import org.specs2.mock.Mockito

import scala.concurrent.Future
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class UserServiceSpec extends PlaySpecification with Mockito {
  isolated
  "UserService" should {
    val mockDAO = mock[UserDAO]
    val userService = new UserServiceImpl(mockDAO)

    val user = new User(userID = UUID.randomUUID(),
      loginInfo = new LoginInfo(providerID = "provider", providerKey = "key"),
      firstName = Some("first"),
      lastName = Some("last"),
      fullName = None,
      email = None,
      avatarURL = None,
      activated = false)


    "retrieve a user by id" in{
      val aUUID = UUID.randomUUID()

      when(mockDAO.find(aUUID)).thenReturn(Future(Some(user)))
      val rslt = Await.result(userService.retrieve(aUUID), 5 seconds)
      verify(mockDAO).find(aUUID)
      rslt should not be empty
      rslt should be equalTo(Some(user))
    }

    "retrieve a user by Login info" in{
      val loginInfo = new LoginInfo(providerID = "provider", providerKey = "key")

      when(mockDAO.find(loginInfo)).thenReturn(Future(Some(user)))
      val rslt = Await.result(userService.retrieve(loginInfo), 5 seconds)
      verify(mockDAO).find(loginInfo)
      rslt should not be empty
      rslt should be equalTo(Some(user))
    }

    "save a user" in{
      when(mockDAO.save(user)).thenReturn(Future(user))

      val rslt = Await.result(userService.save(user), 5 seconds)
      verify(mockDAO).save(user)
      rslt should not be empty
      rslt should be equalTo(user)

    }

    "save profile" can{
      val profile = new CommonSocialProfile(
        loginInfo = new LoginInfo(providerID = "provider", providerKey = "key"),
        firstName = Some("socialfirst"),
        lastName = Some("sociallast"),
        fullName = Some("Mr Social"),
        email = Some("mr@social.net"),
        avatarURL = Some("http:/foo.bar"))


      "add new user" in{
        val captor = capture[User]

        when(mockDAO.save(any[User])).thenReturn(Future(user))
        when(mockDAO.find(profile.loginInfo)).thenReturn(Future(None))
        val rslt = Await.result(userService.save(profile), 5 seconds)

        there was one(mockDAO).save(captor)
        captor.value.userID should not be empty
        captor.value.loginInfo should be equalTo(profile.loginInfo)
        captor.value.firstName should be equalTo(Some("socialfirst"))
        captor.value.lastName should be equalTo(Some("sociallast"))
        captor.value.fullName should be equalTo(Some("Mr Social"))
        captor.value.email should be equalTo(Some("mr@social.net"))
        captor.value.avatarURL should be equalTo(Some("http:/foo.bar"))
      }

      "update existing user" in{
        val captor = capture[User]

        when(mockDAO.save(any[User])).thenReturn(Future(user))
        when(mockDAO.find(profile.loginInfo)).thenReturn(Future(Some(user)))
        val rslt = Await.result(userService.save(profile), 5 seconds)

        there was one(mockDAO).save(captor)
        captor.value.userID should be equalTo(user.userID)
        captor.value.loginInfo should be equalTo(profile.loginInfo)
        captor.value.firstName should be equalTo(Some("socialfirst"))
        captor.value.lastName should be equalTo(Some("sociallast"))
        captor.value.fullName should be equalTo(Some("Mr Social"))
        captor.value.email should be equalTo(Some("mr@social.net"))
        captor.value.avatarURL should be equalTo(Some("http:/foo.bar"))
      }
    }
  }

}
