package services

import models.DeploymentFormData
import play.api.libs.ws.{WSAuthScheme, WSBody, WSClient, WSRequest, WSResponse}
import play.api.test.PlaySpecification

import scala.concurrent._
import scala.concurrent.duration._
import org.specs2.mock._
import play.api.libs.ws.JsonBodyWritables._

import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global

class ClusterStatsServiceSpec extends PlaySpecification with Mockito {

  "Given a cluster stats service" >> {

    "When I request the list of services" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val authRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val clusterStatsService = new ClusterstatsService()

      ws.url("http://localhost/services?full=1") returns(mockRequest)

      mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

      authRequest.get() returns Future(mockResponse)
      mockResponse.status returns 200
      mockResponse.json returns Json.parse("""
          |{
          |  "18koitgrm7": {
          | "containers": [ ],
          |    "cores": 1,
          |    "disk": {
          |      "data": 2,
          |      "used": 3
          |    },
          |    "env": [ ],
          |    "image": "ncsapolyglot/converters-avconv:latest",
          |    "memory": 4,
          |    "name": "dev-converter-avconv",
          |    "nodes": [ ]
          |  }
          | }
        """.stripMargin)


      val resultFuture = clusterStatsService.getServices(ws, "http://test:pass@localhost")

      "Then I should get map of running services" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo(1)

        there was one(ws).url("http://localhost/services?full=1")
        there was one(mockRequest).withAuth("test", "pass", WSAuthScheme.BASIC)
        there was one(authRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json
      }
    }

    "When the response is invalid" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val authRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val clusterStatsService = new ClusterstatsService()

      ws.url("http://localhost/services?full=1") returns(mockRequest)

      mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

      authRequest.get() returns Future(mockResponse)
      mockResponse.status returns 200
      mockResponse.json returns Json.parse("""
                                             |{
                                             |  "18koitgrm7": {
                                             | "containers": [
                                             |   {"id": "314159",
                                             |    "name": "aContainer"}
                                             | ]
                                             |  }
                                             | }
                                           """.stripMargin)


      val resultFuture = clusterStatsService.getServices(ws, "http://test:pass@localhost")

      "Then I should get an empty map back" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo(0)

        there was one(ws).url("http://localhost/services?full=1")
        there was one(mockRequest).withAuth("test", "pass", WSAuthScheme.BASIC)
        there was one(authRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json

      }
    }


  "When the clusterscale service is down" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val authRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]


    val clusterStatsService = new ClusterstatsService()

    ws.url("http://localhost/services?full=1") returns(mockRequest)

    mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

    authRequest.get() returns Future(mockResponse)
    mockResponse.status returns 404


    val resultFuture = clusterStatsService.getServices(ws, "http://test:pass@localhost")

    "Then I should get an empty map back" >> {
      val result = Await.result(resultFuture, 10 seconds)
      result.size must be equalTo(0)

      there was one(ws).url("http://localhost/services?full=1")
      there was one(mockRequest).withAuth("test", "pass", WSAuthScheme.BASIC)
      there was one(authRequest).get()

      there was one(mockResponse).status
      there was no(mockResponse).json

    }
  }

    "and there are extractors" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val authRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val clusterStatsService = new ClusterstatsService()

      ws.url("http://localhost/services?full=1") returns(mockRequest)

      mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

      authRequest.get() returns Future(mockResponse)
      mockResponse.status returns 200
      mockResponse.json returns Json.parse("""
                                             |{
                                             |  "18koitgrm7": {
                                             | "containers": [ ],
                                             |    "cores": 1,
                                             |    "disk": {
                                             |      "data": 2,
                                             |      "used": 3
                                             |    },
                                             |    "labels": {
                                             |      "bd.level": "prod",
                                             |      "bd.rabbitmq.queue": "Clowder",
                                             |      "bd.rabbitmq.vhost": "dap-prod",
                                             |      "bd.replicas.max": "5",
                                             |      "bd.replicas.min": "1",
                                             |      "bd.type": "extractor"
                                             |    },
                                             |    "env": [ ],
                                             |    "image": "ncsapolyglot/converters-avconv:latest",
                                             |    "memory": 4,
                                             |    "name": "myExtractor",
                                             |    "nodes": [ ]
                                             |  },
                                             |    "189koitgrm9": {
                                             | "containers": [ ],
                                             |    "cores": 1,
                                             |    "disk": {
                                             |      "data": 2,
                                             |      "used": 3
                                             |    },
                                             |    "labels": {
                                             |      "bd.level": "prod",
                                             |      "bd.rabbitmq.queue": "Clowder",
                                             |      "bd.rabbitmq.vhost": "dap-prod",
                                             |      "bd.replicas.max": "5",
                                             |      "bd.replicas.min": "1",
                                             |      "bd.type": "converter"
                                             |    },
                                             |    "env": [ ],
                                             |    "image": "ncsapolyglot/converters-avconv:latest",
                                             |    "memory": 4,
                                             |    "name": "dev-converter-avconv",
                                             |    "nodes": [ ]
                                             |  }
                                             | }
                                           """.stripMargin)

      "when I request extractors" >> {
        val resultFuture = clusterStatsService.getExtractors(ws, "http://test:pass@localhost")
        "then I should get them back" >> {
          val result = Await.result(resultFuture, 5 seconds)
          result.size must be equalTo(1)
          result.head.service.name must be equalTo("myExtractor")
        }
      }
    }

    "and there are extractors" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val authRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val clusterStatsService = new ClusterstatsService()

      ws.url("http://localhost/services?full=1") returns (mockRequest)

      mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

      authRequest.get() returns Future(mockResponse)
      mockResponse.status returns 200
      mockResponse.json returns Json.parse(
        """
                                             |{
                                             |  "18koitgrm7": {
                                             | "containers": [ ],
                                             |    "cores": 1,
                                             |    "disk": {
                                             |      "data": 2,
                                             |      "used": 3
                                             |    },
                                             |    "labels": {
                                             |      "bd.level": "prod",
                                             |      "bd.rabbitmq.queue": "Clowder",
                                             |      "bd.rabbitmq.vhost": "dap-prod",
                                             |      "bd.replicas.max": "5",
                                             |      "bd.replicas.min": "1",
                                             |      "bd.type": "extractor"
                                             |    },
                                             |    "env": [ ],
                                             |    "image": "ncsapolyglot/converters-avconv:latest",
                                             |    "memory": 4,
                                             |    "name": "myExtractor",
                                             |    "nodes": [ ]
                                             |  },
                                             |    "18koitgrm8": {
                                             | "containers": [ ],
                                             |    "cores": 1,
                                             |    "disk": {
                                             |      "data": 2,
                                             |      "used": 3
                                             |    },
                                             |    "labels": {
                                             |      "bd.level": "prod",
                                             |      "bd.rabbitmq.queue": "Clowder",
                                             |      "bd.rabbitmq.vhost": "dap-prod2",
                                             |      "bd.replicas.max": "5",
                                             |      "bd.replicas.min": "1",
                                             |      "bd.type": "converter"
                                             |    },
                                             |    "env": [ ],
                                             |    "image": "ncsapolyglot/converters-avconv:latest",
                                             |    "memory": 4,
                                             |    "name": "dev-converter-avconv",
                                             |    "nodes": [ ]
                                             |  }
                                             | }
                                           """.stripMargin)

      "when I request extractors" >> {
        val resultFuture = clusterStatsService.getExtractors(ws, "http://test:pass@localhost")

        "then I should get them back" >> {
          val result = Await.result(resultFuture, 5 seconds)
          result.size must be equalTo(1)
          result.head.service.name must be equalTo("myExtractor")
        }
      }
      }

    "and there are extractors with different vhosts" >> {

      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val authRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val clusterStatsService = new ClusterstatsService()

      ws.url("http://localhost/services?full=1") returns (mockRequest)

      mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest

      authRequest.get() returns Future(mockResponse)
      mockResponse.status returns 200
      mockResponse.json returns Json.parse(
        """
          |{
          |  "18koitgrm7": {
          | "containers": [ ],
          |    "cores": 1,
          |    "disk": {
          |      "data": 2,
          |      "used": 3
          |    },
          |    "labels": {
          |      "bd.level": "prod",
          |      "bd.rabbitmq.queue": "Clowder",
          |      "bd.rabbitmq.vhost": "dap-prod",
          |      "bd.replicas.max": "5",
          |      "bd.replicas.min": "1",
          |      "bd.type": "extractor"
          |    },
          |    "env": [ ],
          |    "image": "ncsapolyglot/converters-avconv:latest",
          |    "memory": 4,
          |    "name": "myExtractor",
          |    "nodes": [ ]
          |  },
          |    "18koitgrm8": {
          | "containers": [ ],
          |    "cores": 1,
          |    "disk": {
          |      "data": 2,
          |      "used": 3
          |    },
          |    "labels": {
          |      "bd.level": "prod",
          |      "bd.rabbitmq.queue": "Clowder",
          |      "bd.rabbitmq.vhost": "dap-prod2",
          |      "bd.replicas.max": "5",
          |      "bd.replicas.min": "1",
          |      "bd.type": "extractor"
          |    },
          |    "env": [ ],
          |    "image": "ncsapolyglot/converters-avconv:latest",
          |    "memory": 4,
          |    "name": "dev-converter-avconv",
          |    "nodes": [ ]
          |  }
          | }
        """.stripMargin)

      "when I request extractors by vhost" >> {
        val resultFuture = clusterStatsService.getExtractorsForVhost(ws, "http://test:pass@localhost", "dap-prod")

        "then I should get them back" >> {
          val result = Await.result(resultFuture, 5 seconds)
          result.size must be equalTo(1)
          result.head.service.name must be equalTo("myExtractor")
        }
      }
    }

    "And I have a good request" >> {
      "When I deploy a tool" >> {
        val ws = mock[WSClient]
        val mockRequest = mock[WSRequest]
        val authRequest = mock[WSRequest]
        val mockResponse = mock[WSResponse]


        val clusterStatsService = new ClusterstatsService()

        ws.url("http://localhost/services") returns (mockRequest)

        mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest


        val labels = models.Label("4", "queue", "vhost", 1, 0, "extractor", Some("42"))
        val resources = models.Resources(None, None)
        val deployParams = models.DeployParams("name", "image", Seq("a", "b"), labels, resources)

        authRequest.post(Json.toJson(deployParams)) returns Future(mockResponse)
        mockResponse.status returns 200

        val resultFut = clusterStatsService.deploy(ws, "http://test:pass@localhost", deployParams)

        "Then it should report success" >> {
          val result = Await.result(resultFut, 5 seconds)
          result must beSuccessfulTry[String]
        }

      }

    }

    "And I have an invalid request" >> {
      "When I deploy a tool" >> {
        val ws = mock[WSClient]
        val mockRequest = mock[WSRequest]
        val authRequest = mock[WSRequest]
        val mockResponse = mock[WSResponse]


        val clusterStatsService = new ClusterstatsService()

        ws.url("http://localhost/services") returns (mockRequest)

        mockRequest.withAuth("test", "pass", WSAuthScheme.BASIC) returns authRequest


        val labels = models.Label("4", "queue", "vhost", 1, 0, "extractor", Some("42"))
        val resources = models.Resources(None, None)
        val deployParams = models.DeployParams("name", "image", Seq("a", "b"), labels, resources)

        authRequest.post(Json.toJson(deployParams)) returns Future(mockResponse)
        mockResponse.status returns 500
        mockResponse.body returns "ouch"

        val resultFut = clusterStatsService.deploy(ws, "http://test:pass@localhost", deployParams)

        "Then it should report failure" >> {
          val result = Await.result(resultFut, 5 seconds)
          result must beAFailedTry[String]
        }

      }

    }

  }

}
