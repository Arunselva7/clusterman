package services

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import models.{AuthToken, User}
import models.daos.{AuthTokenDAO, UserDAO}
import org.mockito.Mockito._
import play.api.test.PlaySpecification
import org.specs2.mock.Mockito
import com.mohiva.play.silhouette.api.util.Clock
import org.joda.time.{DateTime, DateTimeZone}
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

import scala.concurrent.Future
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class AuthTokenServiceSpec extends PlaySpecification with Mockito{
  isolated

  "authTokenService" should {
    val mockDAO = mock[AuthTokenDAO]
    val authTokenService = new AuthTokenServiceImpl(mockDAO, Clock.apply())
    val uuid = UUID.randomUUID()


    "create new instance" in{
      val captor = capture[AuthToken]
      val expire = Clock.apply().now.withZone(DateTimeZone.UTC).plusMinutes(1)

      when(mockDAO.save(any[AuthToken])).thenAnswer((invocation: InvocationOnMock) =>
          Future(invocation.getArguments()(0).asInstanceOf[AuthToken])
      )

      val result = Await.result(authTokenService.create(uuid, 1 minute), 5 seconds)
      result should not be empty

      there was one(mockDAO).save(captor)
      captor.value.userID should be equalTo(uuid)
      Math.abs(captor.value.expiry.getMillis -  expire.getMillis) should be lessThan(5000)

    }

    "validate an existing instance" in {
      val authToken =  AuthToken(UUID.randomUUID(), uuid, Clock.apply.now.withZone(DateTimeZone.UTC).plusSeconds(60))
      when(mockDAO.find(uuid)).thenReturn(Future(Some(authToken)))

      val result = Await.result(authTokenService.validate(uuid), 5 seconds)
      there was one(mockDAO).find(uuid)
      result.get should be equalTo(authToken)
    }

    "remove token upon expiry" in {
      val authID = UUID.randomUUID()
      val authToken =  AuthToken(authID, uuid, Clock.apply.now.withZone(DateTimeZone.UTC).plusSeconds(60))
      val expire = Clock.apply().now.withZone(DateTimeZone.UTC)
      when(mockDAO.findExpired(any[DateTime])).thenReturn(Future(Seq(authToken)))
      when(mockDAO.remove(authID)).thenReturn(Future.successful(()))

      val captor = capture[UUID]

      Await.result(authTokenService.clean, 5 seconds)
      there was one(mockDAO).remove(captor)
      captor.value should be equalTo(authID)
    }
  }


}
