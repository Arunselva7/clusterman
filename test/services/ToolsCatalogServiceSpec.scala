package services

import models.ToolsCatalogRecord
import org.specs2.mock._
import play.api.libs.json.Json
import play.api.libs.ws.{WSAuthScheme, WSClient, WSRequest, WSResponse}
import play.api.test.PlaySpecification

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._

class ToolsCatalogServiceSpec extends PlaySpecification with Mockito {

  "Given a ToolsCatalogservice" >> {

    "When I request the list of extractors" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val toolsCatalogService = new ToolsCatalogService()

      ws.url("http://localhost:9001/api/v1/extractors") returns(mockRequest)
      mockRequest.get() returns(Future(mockResponse))

      mockResponse.status returns 200
      mockResponse.json returns Json.parse(
        """
          |[
          |  {
          |    "tool": {
          |      "toolId": "5a67730bcc0100cc01134d9d",
          |      "title": "Image GeoTiff",
          |      "url": "https://trac.osgeo.org/geotiff/",
          |      "shortDesc": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file.",
          |      "description": "did",
          |      "citation": "adds",
          |      "video": "http://localhost:9001/tools/edit/5a67730bcc0100cc01134d9d",
          |      "toolType": "extractor",
          |      "toolLevel": 3,
          |      "author": "guest"
          |    },
          |    "toolVersion": {
          |      "toolId": "5a67730bcc0100cc01134d9d",
          |      "toolVersionId": "5a677326cc0100cc01134d9e",
          |      "version": "Image GeoTiff",
          |      "url": "",
          |      "params": {
          |        "dockerfile": "clower/der",
          |        "sampleInput": "",
          |        "sampleOutput": "",
          |        "extractorName": "geotiff",
          |        "dockerimageName": "ncsa/clowder-ocr:jsonld",
          |        "extractorDef": "",
          |        "queueName": "geotiff"
          |      },
          |      "author": "guest",
          |      "vmImageName": "",
          |      "status": 4,
          |      "interfaceLevel": 3,
          |      "creationDate": "2018-01-23T11:38:46.886-06:00",
          |      "updateDate": "2018-01-23T11:38:46.886-06:00",
          |      "downloads": 0,
          |      "whatsNew": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file. This extractor uses the python modules GDAL and pygeoprocessing.",
          |      "compatibility": "",
          |      "reviews": null
          |    }
          |  }
          |]
          """
        .stripMargin)


      val resultFuture = toolsCatalogService.getExtractors(ws, "http://localhost:9001")

      "Then I should get list of extractors" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo(1)

        there was one(ws).url("http://localhost:9001/api/v1/extractors")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json
      }
    }


    "When I request the list of converters" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val toolsCatalogService = new ToolsCatalogService()

      ws.url("http://localhost:9001/api/v1/converters") returns(mockRequest)
      mockRequest.get() returns(Future(mockResponse))

      mockResponse.status returns 200
      mockResponse.json returns Json.parse(
        """
          |[
          |  {
          |    "tool": {
          |      "toolId": "5a67730bcc0100cc01134d9d",
          |      "title": "Image GeoTiff",
          |      "url": "https://trac.osgeo.org/geotiff/",
          |      "shortDesc": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file.",
          |      "description": "did",
          |      "citation": "adds",
          |      "video": "http://localhost:9001/tools/edit/5a67730bcc0100cc01134d9d",
          |      "toolType": "extractor",
          |      "toolLevel": 3,
          |      "author": "guest"
          |    },
          |    "toolVersion": {
          |      "toolId": "5a67730bcc0100cc01134d9d",
          |      "toolVersionId": "5a677326cc0100cc01134d9e",
          |      "version": "Image GeoTiff",
          |      "url": "",
          |      "params": {
          |        "dockerfile": "clower/der",
          |        "sampleInput": "",
          |        "sampleOutput": "",
          |        "extractorName": "geotiff",
          |        "dockerimageName": "ncsa/clowder-ocr:jsonld",
          |        "extractorDef": "",
          |        "queueName": "geotiff"
          |      },
          |      "author": "guest",
          |      "vmImageName": "",
          |      "status": 4,
          |      "interfaceLevel": 3,
          |      "creationDate": "2018-01-23T11:38:46.886-06:00",
          |      "updateDate": "2018-01-23T11:38:46.886-06:00",
          |      "downloads": 0,
          |      "whatsNew": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file. This extractor uses the python modules GDAL and pygeoprocessing.",
          |      "compatibility": "",
          |      "reviews": null
          |    }
          |  }
          |]
        """
          .stripMargin)



      val resultFuture = toolsCatalogService.getConverters(ws, "http://localhost:9001")

      "Then I should get list of converters" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo(1)

        there was one(ws).url("http://localhost:9001/api/v1/converters")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json
      }
    }


    "When the response is invalid" >> {
      val ws = mock[WSClient]
      val mockRequest = mock[WSRequest]
      val mockResponse = mock[WSResponse]


      val toolsCatalogService = new ToolsCatalogService()
      ws.url("http://localhost:9001/api/v1/extractors") returns(mockRequest)
      mockRequest.get() returns(Future(mockResponse))

      mockResponse.status returns 200
      mockResponse.json returns Json.parse(
        """
          |[
          |    {
          |        "toolId": "5a67730bcc0100cc01134d9d",
          |        "reviews": null
          |    }
          |]
        """.stripMargin)


      val resultFuture = toolsCatalogService.getExtractors(ws, "http://localhost:9001")

      "Then I should get an empty list back" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo(0)

        there was one(ws).url("http://localhost:9001/api/v1/extractors")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json

      }
    }

  }

  "When the ToolsCatalog is down" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]

    val toolsCatalogService = new ToolsCatalogService()
    ws.url("http://localhost:9001/api/v1/extractors") returns(mockRequest)
    mockRequest.get() returns(Future(mockResponse))

    mockResponse.status returns 404


    val resultFuture = toolsCatalogService.getExtractors(ws, "http://localhost:9001")

    "Then I should get an empty map back" >> {
      val result = Await.result(resultFuture, 10 seconds)
      result.size must be equalTo(0)

      there was one(ws).url("http://localhost:9001/api/v1/extractors")
      there was one(mockRequest).get()

      there was one(mockResponse).status
      there was no(mockResponse).json
    }
  }

  "When I request the extractor def for an extractor" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]


    val toolsCatalogService = new ToolsCatalogService()

    ws.url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig") returns(mockRequest)
    mockRequest.get() returns(Future(mockResponse))

    mockResponse.status returns 200
    mockResponse.json returns  Json.parse(
      """
        |{
        |  "@context": "http://clowder.ncsa.illinois.edu/contexts/extractors.jsonld",
        |  "name": "ncsa.wordcount",
        |  "version": "2.0",
        |  "description": "WordCount extractor",
        |  "author": "Rob Kooper <kooper@illinois.edu>",
        |  "contributors": ["Bob", "Mary"],
        |  "contexts": [ ],
        |  "repository": [
        |  {
        |    "repType": "git",
        |    "repUrl": "https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git"
        |  }
        |  ],
        |  "process": {
        |    "file": [
        |      "text/*",
        |      "application/json"
        |    ]
        |  },
        |  "external_services": ["a"],
        |  "dependencies": ["b"],
        |  "bibtex": ["c"]
        |}
      """.stripMargin)

    val resultFuture = toolsCatalogService.getExtractorInfo(ws, "http://localhost:9001", "123456")

    "Then I should get the extractor info" >> {
      val result = Await.result(resultFuture, 10 seconds)

      there was one(ws).url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig")
      there was one(mockRequest).get()

      there was one(mockResponse).status
      there was one(mockResponse).json
    }
  }

  "When the response is invalid" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]


    val toolsCatalogService = new ToolsCatalogService()
    ws.url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig") returns(mockRequest)
    mockRequest.get() returns(Future(mockResponse))

    mockResponse.status returns 200
    mockResponse.json returns Json.parse(
      """
        |    {
        |        "name": "5a67730bcc0100cc01134d9d",
        |        "reviews": null
        |    }
      """.stripMargin)


    val resultFuture = toolsCatalogService.getExtractorInfo(ws, "http://localhost:9001", "123456")

    "Then I should None back" >> {
      val result = Await.result(resultFuture, 10 seconds)
      result must beNone

      there was one(ws).url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig")
      there was one(mockRequest).get()

      there was one(mockResponse).status
      there was one(mockResponse).json

    }
  }

  "Given the ToolsCatalog is down" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]

    val toolsCatalogService = new ToolsCatalogService()
    ws.url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig") returns (mockRequest)
    mockRequest.get() returns (Future(mockResponse))

    mockResponse.status returns 404

    "When I request extractor info" >> {
      val resultFuture = toolsCatalogService.getExtractorInfo(ws, "http://localhost:9001", "123456")

      "Then I should get None back" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo (0)

        there was one(ws).url("http://localhost:9001/api/v1/toolVersions/123456/extractorConfig")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was no(mockResponse).json
      }
    }
  }

  "Given there is a tool in the tools catalog" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]


    val toolsCatalogService = new ToolsCatalogService()

    ws.url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e") returns(mockRequest)
    mockRequest.get() returns(Future(mockResponse))

    mockResponse.status returns 200
    mockResponse.json returns Json.parse(
      """
        |  {
        |    "tool": {
        |      "toolId": "5a67730bcc0100cc01134d9d",
        |      "title": "Image GeoTiff",
        |      "url": "https://trac.osgeo.org/geotiff/",
        |      "shortDesc": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file.",
        |      "description": "did",
        |      "citation": "adds",
        |      "video": "http://localhost:9001/tools/edit/5a67730bcc0100cc01134d9d",
        |      "toolType": "extractor",
        |      "toolLevel": 3,
        |      "author": "guest"
        |    },
        |    "toolVersion": {
        |      "toolId": "5a67730bcc0100cc01134d9d",
        |      "toolVersionId": "5a677326cc0100cc01134d9e",
        |      "version": "Image GeoTiff",
        |      "url": "",
        |      "params": {
        |        "dockerfile": "clower/der",
        |        "sampleInput": "",
        |        "sampleOutput": "",
        |        "extractorName": "geotiff",
        |        "dockerimageName": "ncsa/clowder-ocr:jsonld",
        |        "extractorDef": "",
        |        "queueName": "geotiff"
        |      },
        |      "author": "guest",
        |      "vmImageName": "",
        |      "status": 4,
        |      "interfaceLevel": 3,
        |      "creationDate": "2018-01-23T11:38:46.886-06:00",
        |      "updateDate": "2018-01-23T11:38:46.886-06:00",
        |      "downloads": 0,
        |      "whatsNew": "GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded within a TIFF file. This extractor uses the python modules GDAL and pygeoprocessing.",
        |      "compatibility": "",
        |      "reviews": null
        |    }
        |  }
      """
        .stripMargin)

    "When I request info on it" >> {
      val resultFuture = toolsCatalogService.getTool(ws, "http://localhost:9001", "5a677326cc0100cc01134d9e")

      "Then I get back the object" >> {
         val result = Await.result(resultFuture, 10 seconds)
          result must beSome[ToolsCatalogRecord]
          there was one(ws).url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e")
          there was one(mockRequest).get()

          there was one(mockResponse).status
          there was one(mockResponse).json
        }
      }
    }

  "Given there is an invalid tool in the tools catalog" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]


    val toolsCatalogService = new ToolsCatalogService()

    ws.url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e") returns(mockRequest)
    mockRequest.get() returns(Future(mockResponse))

    mockResponse.status returns 200
    mockResponse.json returns Json.parse(
      """
        |    {
        |        "toolId": "5a67730bcc0100cc01134d9d"
        |    }
      """
        .stripMargin)
    "When I request info on it" >> {
      val resultFuture = toolsCatalogService.getTool(ws, "http://localhost:9001", "5a677326cc0100cc01134d9e")

      "Then I get back None" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result must beNone
        there was one(ws).url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was one(mockResponse).json
      }
    }
  }

  "Given the ToolsCatalog is down" >> {
    val ws = mock[WSClient]
    val mockRequest = mock[WSRequest]
    val mockResponse = mock[WSResponse]

    val toolsCatalogService = new ToolsCatalogService()
    ws.url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e") returns (mockRequest)
    mockRequest.get() returns (Future(mockResponse))

    mockResponse.status returns 404

    "When I request extractor info" >> {
      val resultFuture = toolsCatalogService.getTool(ws, "http://localhost:9001", "5a677326cc0100cc01134d9e")

      "Then I should get None back" >> {
        val result = Await.result(resultFuture, 10 seconds)
        result.size must be equalTo (0)

        there was one(ws).url("http://localhost:9001/api/v1/toolVersions/5a677326cc0100cc01134d9e")
        there was one(mockRequest).get()

        there was one(mockResponse).status
        there was no(mockResponse).json
      }
    }
  }


}
