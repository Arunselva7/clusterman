package models

import play.api.libs.json.{JsSuccess, Json}
import play.api.test.PlaySpecification

class ExtractorInfoSpec extends PlaySpecification{
  "Given a json document" >> {
    val doc = Json.parse(
      """
      |{
      |  "@context": "http://clowder.ncsa.illinois.edu/contexts/extractors.jsonld",
      |  "name": "ncsa.wordcount",
      |  "version": "2.0",
      |  "description": "WordCount extractor",
      |  "author": "Rob Kooper <kooper@illinois.edu>",
      |  "contributors": ["Bob", "Mary"],
      |  "contexts": [
      |    {
      |      "lines": "http://clowder.ncsa.illinois.edu/metadata/ncsa.wordcount#lines",
      |      "words": "http://clowder.ncsa.illinois.edu/metadata/ncsa.wordcount#words",
      |      "characters": "http://clowder.ncsa.illinois.edu/metadata/ncsa.wordcount#characters"
      |    }
      |  ],
      |  "repository": [
      |  {
      |    "repType": "git",
      |    "repUrl": "https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git"
      |  }
      |  ],
      |  "process": {
      |    "file": [
      |      "text/*",
      |      "application/json"
      |    ],
      |    "datastore":[
      |       "ds1",
      |       "ds2"
      |    ]
      |  },
      |  "external_services": ["a"],
      |  "dependencies": ["b"],
      |  "bibtex": ["c"]
      |}
    """.stripMargin)
    "when I read it" >> {
      val jsResult = ExtractorInfo.extractorInfoReads.reads(doc)
      "then the object should be populated" >> {
        jsResult must haveClass[JsSuccess[ExtractorInfo]]
        val record = jsResult.get
        record.name must be equalTo("ncsa.wordcount")
        record.version must be equalTo("2.0")
        record.description must be equalTo("WordCount extractor")
        record.author must be equalTo("Rob Kooper <kooper@illinois.edu>")
        record.contributors(0) must be equalTo("Bob")
        record.contributors(1) must be equalTo("Mary")
        record.repository.size must be equalTo(1)
        record.repository(0).repType must be equalTo("git")
        record.repository(0).repUrl must be equalTo("https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git")
        record.external_services(0) must be equalTo("a")
        record.dependencies(0) must be equalTo("b")
        record.bibtex(0) must be equalTo("c")

        record.process("file").processKey must be equalTo("file")
        record.process("file").mimeTypes.size must be equalTo(2)
        record.process("file").mimeTypes(0) must be equalTo("text/*")
        record.process("file").mimeTypes(1) must be equalTo("application/json")

        record.process("datastore").processKey must be equalTo("datastore")
        record.process("datastore").mimeTypes(0) must be equalTo("ds1")
        record.process("datastore").mimeTypes(1) must be equalTo("ds2")
      }
    }
    }
}
