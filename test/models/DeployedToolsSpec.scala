package models

import play.api.test.PlaySpecification
import play.api.libs.json.Json

class DeployedToolsSpec extends PlaySpecification {

  "Given a json record" >> {
    val doc = Json.parse(
      """
        |{
        | "containers": [
        |   {"id": "314159",
        |    "name": "aContainer"}
        | ],
        |    "cores": 1,
        |    "disk": {
        |      "data": 2,
        |      "used": 3
        |    },
        |    "env": [
        |      "RABBITMQ_URI=amqp://dap-dev:polyglot@rabbitmq.ncsa.illinois.edu:5672/dap-dev"
        |    ],
        |    "image": "ncsapolyglot/converters-avconv:latest",
        |    "labels": {
        |      "bd.level": "dev",
        |      "bd.rabbitmq.queue": "avconv",
        |      "bd.rabbitmq.vhost": "dap-dev",
        |      "bd.replicas.max": "5",
        |      "bd.replicas.min": "1",
        |      "bd.type": "converter",
        |      "project": "BrownDog",
        |      "staging": "dev",
        |      "bd.toolcatalog.id": "42"
        |    },
        |    "memory": 4,
        |    "name": "dev-converter-avconv",
        |    "nodes": [
        |       {"id": "1234",
        |        "name": "aNode"}
        |    ],
        |    "replicas": {
        |      "requested": 5,
        |      "running": 6
        |    }
        |  }
      """.stripMargin)

    "when I read it" >> {
      val service = Service.serviceReads.reads(doc).get

      "then I get a project object"   >> {
        service.name must be equalTo ("dev-converter-avconv")
        service.cores must be equalTo (1)
        service.disk.data must be equalTo (2)
        service.disk.used must be equalTo (3)
        service.image must be equalTo ("ncsapolyglot/converters-avconv:latest")
        service.memory must be equalTo (4)
        service.replicas must beSome
        service.replicas.get.requested must be equalTo (5)
        service.replicas.get.requested must be equalTo (5)

        service.env.length must be equalTo (1)
        service.env.head must be equalTo ("RABBITMQ_URI=amqp://dap-dev:polyglot@rabbitmq.ncsa.illinois.edu:5672/dap-dev")

        service.labels must beSome
        service.labels.get.bdType must be equalTo ("converter")
        service.labels.get.level must be equalTo ("dev")
        service.labels.get.queue must be equalTo ("avconv")
        service.labels.get.replicasMax must be equalTo (5)
        service.labels.get.replicasMin must be equalTo (1)
        service.labels.get.scriptID.get must be equalTo ("42")
        service.labels.get.vhost must be equalTo ("dap-dev")

        service.containers.length must be equalTo (1)
        service.containers.head.name must be equalTo ("aContainer")
        service.containers.head.id must be equalTo ("314159")

        service.nodes.length must be equalTo (1)
        service.nodes.head.id must be equalTo ("1234")
        service.nodes.head.name must be equalTo ("aNode")

      }
    }
  }
}
