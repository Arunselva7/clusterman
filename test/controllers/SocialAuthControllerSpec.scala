package controllers
import java.util.UUID

import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.util.{ExtractableRequest, HTTPLayer}
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.providers.oauth2.GoogleProvider
import com.mohiva.play.silhouette.test._
import models.User
import models.daos.{ProjectDAO, ProjectDAOImpl}
import services.UserService
import net.codingwell.scalaguice.ScalaModule
import org.mockito.Matchers.{eq => eqTo}
import org.mockito.Mockito._
import org.specs2.mock.Mockito
import org.specs2.specification.Scope
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, _}
import play.api.mvc._
import play.api.test.CSRFTokenHelper._
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}
import reactivemongo.api.DefaultDB
import services.RabbitAdminService
import utils.auth.{DefaultEnv, LDAPInfo, LDAPProvider, LDAPSettings}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, _}
import scala.concurrent.duration._
import scala.reflect.ClassTag

/**
  * Created by ben on 11/9/17.
  */
class SocialAuthControllerSpec extends PlaySpecification with Mockito {
  "social auth login" should {

    "if user choose LDAP, redirect to login page" in new Context {
      new WithApplication(application) {
        val httpLayer:HTTPLayer = {
          val m = mock[HTTPLayer]
          //      m.executionContext returns global
          m
        }

        val ldapSettings = spy(LDAPSettings(
          hostname = "https://steamcommunity.com/openid/",
          group = "steam",
          port = 9000,
          baseDN = "aa",
          userDN = "aa",
          groupDN = "aa",
          objectClass = "aa",
          trustAllCertificates = true
        ))

        val ldapProvider = new LDAPProvider(httpLayer, ldapSettings)
        when(mockProvider.get(eqTo("LDAP"))(any[ClassTag[SocialProvider]])).thenReturn(Some(ldapProvider))
        when(mockUserService.save(profile)).thenReturn(Future(identity))

        val Some(result) = route(app, addCSRFToken(FakeRequest(routes.SocialAuthController.authenticate("LDAP"))
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )

        val foo = Await.result(result, 10 seconds)
        foo.header.status must be equalTo SEE_OTHER

        val redirectURL = redirectLocation(result).getOrElse("")
        redirectURL must be equalTo("ldapinput")

        val Some(result2) = route(app, addCSRFToken(FakeRequest(routes.SocialAuthController.authenticate("ldapinput"))
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )

        status(result2) must be equalTo 200
        contentType(result2) must beSome.which(_ == "text/html")
      }
    }

    "POST to ldapauthenticate and redirect to home page" in new Context {
      new WithApplication(application) {
        val ldapProvider = mock[LDAPProvider]
        val authInfo = new LDAPInfo(code="loginhtml", username="aa", password="aa")

        when(ldapProvider.authenticate()(any[ExtractableRequest[JsValue]])).thenReturn(Future(Right(authInfo)))
        when(ldapProvider.retrieveProfile(authInfo)).thenReturn(Future(profile))
        when(mockUserService.save(profile)).thenReturn(Future(identity))
        when(mockProvider.get(eqTo("LDAP"))(any[ClassTag[SocialProvider]])).thenReturn(Some(ldapProvider))

        val Some(resultFailed) =route(app, addCSRFToken(FakeRequest(
          POST, "/authenticate/LDAP").withJsonBody(Json.parse(
          """
            |{
            |"username": "aa",
            |"password": "aa"
            |}
          """.stripMargin
        )).withAuthenticator[DefaultEnv](identity.loginInfo)))
        status(resultFailed) must be equalTo BAD_REQUEST

        val Some(result) =route(app, addCSRFToken(FakeRequest(
          POST, "/authenticate/LDAP").withJsonBody(Json.parse(
          """
            |{
            |"code": "aa",
            |"username": "aa",
            |"password": "aa"
            |}
          """.stripMargin
        )).withAuthenticator[DefaultEnv](identity.loginInfo)))

        status(result) must be equalTo SEE_OTHER

        val redirectURL = redirectLocation(result).getOrElse("")
        redirectURL must be equalTo("/")
      }
    }

    "Ask user to try again if the provider is not in the registry" in new Context {
      new WithApplication(application) {
       val Some(result) = route(app, addCSRFToken(FakeRequest(routes.SocialAuthController.authenticate("mySpace"))
            .withAuthenticator[DefaultEnv](identity.loginInfo)))

        val foo = Await.result(result, 10 seconds)
        foo.header.status must be equalTo SEE_OTHER

        val redirectURL = redirectLocation(result).getOrElse("")
        redirectURL must be equalTo("/signIn")

      }
    }

    "redirect user to social auth providers page if not logged in" in new Context {
      new WithApplication(application) {
        val ldapProvider = mock[LDAPProvider]

        val mockResult = Results.Redirect("foo/bar")

        when(ldapProvider.authenticate()(any[ExtractableRequest[JsValue]])).thenReturn(Future(Left(mockResult)))
        when(mockProvider.get(eqTo("LDAP"))(any[ClassTag[SocialProvider]])).thenReturn(Some(ldapProvider))

        val Some(result) = route(app, addCSRFToken(FakeRequest(routes.SocialAuthController.authenticate("LDAP"))
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )

        val referedPage = Await.result(result, 5 seconds)
        referedPage.header.status must be equalTo(303)
        referedPage.header.headers.get("Location").getOrElse("") must be equalTo("foo/bar")
      }
    }
  }

  /**
    * The context.
    */
  trait Context extends Scope {

    /**
      * A fake Guice module.
      */
    val mockProvider = mock[SocialProviderRegistry]

    val mockUserService = mock[UserService]

    val mockDB = mock[DefaultDB]
    val mockRabbitAdminService = mock[RabbitAdminService]

    class FakeModule extends AbstractModule with ScalaModule {
      def configure() = {
        bind[Environment[DefaultEnv]].toInstance(env)
        bind[SocialProviderRegistry].toInstance(mockProvider)
        bind[UserService].toInstance(mockUserService)
        bind[DefaultDB].toInstance(mockDB)
        bind[ProjectDAO].to[ProjectDAOImpl]
        bind[RabbitAdminService].toInstance(mockRabbitAdminService)
      }
    }

    /**
      * An identity.
      */
    val identity = User(
      userID = UUID.randomUUID(),
      loginInfo = LoginInfo("facebook", "user@facebook.com"),
      firstName = None,
      lastName = None,
      fullName = None,
      email = None,
      avatarURL = None,
      activated = true
    )

    val profile = new CommonSocialProfile(loginInfo = new LoginInfo(providerID = "google", providerKey = "123"),
      firstName = Some("Bob"),
      lastName = Some("Dobo"),
      fullName = Some("Robert Dobo"), email = Some("bob@dobo.com"), avatarURL = None )

    /**
      * A Silhouette fake environment.
      */
    implicit val env: Environment[DefaultEnv] = new FakeEnvironment[DefaultEnv](Seq(identity.loginInfo -> identity))



    /**
      * The application.
      */
    lazy val application = new GuiceApplicationBuilder()
      .overrides(new FakeModule)
      .build()
  }
}


