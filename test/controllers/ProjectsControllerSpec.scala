package controllers

import java.util
import java.util.UUID

import _root_.services.{ClusterstatsService, RabbitAdminService}
import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.test._
import org.mockito.Matchers.{eq => eqTo}
import models._
import models.daos.ProjectDAO
import net.codingwell.scalaguice.ScalaModule
import org.mockito.Matchers.{eq => eqTo}
import org.specs2.mock.Mockito
import org.specs2.specification.Scope
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}
import reactivemongo.api.DefaultDB
import _root_.services.ToolsCatalogService
import play.api.Application
import utils.auth.DefaultEnv

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future} // Oops there is a services package inside models which clashes

class ProjectsControllerSpec extends PlaySpecification with Mockito {
  val mockProjectDAO = mock[ProjectDAO]
  val mockToolsCatalogService = mock[ToolsCatalogService]


  "Given there is a project on the projects tab" >> {
    "When I click on the bind button" >> {
      "Then I should see the bind page for that project" >> new Context {

        val mockRabbitAdminService = mock[RabbitAdminService]
        new WithApplication(app(mockRabbitAdminService)) {
          mockClusterStatsService.getExtractorsForVhost(any[WSClient], eqTo("http://user:pass@localhost:8888"), eqTo("clowder-dev")) returns Future(Iterable[DeployedTool]())
          val project = new Project("myProject", ToolType.Extractor, "myCluster")
          mockProjectDAO.getProjectByName("myCluster", "myProject").returns(Future(Some(project)))


          val bindings = new ProjectBindings(project, Map[String, List[ToolBinding]](
            "just_files"->List(
              new ToolBinding("just_files", "*.file.application.x-7z-compressed"),
              new ToolBinding("just_files","*.file.application.x-zip")
            )))

          mockRabbitAdminService.getProjectBindings("http://user:pass@host:10000", "clowder-dev", project)returns(Future(bindings))


          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(routes.ProjectsController.showBindingsForProject("myCluster", "myProject"))
              .withAuthenticator[DefaultEnv](identity.loginInfo))
          )

          val result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (OK)
          there was one(mockClusterStatsService).getExtractorsForVhost(any[WSClient], eqTo("http://user:pass@localhost:8888"), eqTo("clowder-dev"))
        }
      }
    }
  }

  "Given an invalid cluster setting" >> {
    "When I click on the bind button" >> {
      "Then I should receive an error page" >> new Context {
        new WithApplication(application) {
          mockClusterStatsService.getExtractors(any[WSClient], eqTo("http://user:pass@localhost:8888")) returns Future(Iterable[DeployedTool]())
          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(routes.ProjectsController.showBindingsForProject("XXX-BAD-CLUSTER", "myProject"))
              .withAuthenticator[DefaultEnv](identity.loginInfo))
          )

          val result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (BAD_REQUEST)
          there was no(mockClusterStatsService).getExtractors(any[WSClient], eqTo("http://user:pass@localhost:8888"))
        }
      }
    }
  }

  "Given I am on the edit bindings page" >> {
    "When I click submit" >> {
      val mockRabbitAdminService = mock[RabbitAdminService]
      "Then I should accept my new settings" >> new Context {
        new WithApplication(app(mockRabbitAdminService)) {
          val project = new Project("myProject", ToolType.Extractor, "myCluster")
          mockProjectDAO.getProjectByName("myCluster", "myProject").returns(Future(Some(project)))

          val doc = Json.parse(
            """
              |{
              |  "@context": "http://clowder.ncsa.illinois.edu/contexts/extractors.jsonld",
              |  "name": "ncsa.wordcount",
              |  "version": "2.0",
              |  "description": "WordCount extractor",
              |  "author": "Rob Kooper <kooper@illinois.edu>",
              |  "contributors": ["Bob", "Mary"],
              |  "contexts": [ ],
              |  "repository": [ ],
              |  "process": {
              |    "file": [
              |      "text/*",
              |      "application/json"
              |    ]
              |  },
              |  "external_services": [ ],
              |  "dependencies": [ ],
              |  "bibtex": [ ]
              |}
            """.stripMargin)
            val extractorInfo = ExtractorInfo.extractorInfoReads.reads(doc)

          mockToolsCatalogService.getExtractorInfo(any[WSClient],eqTo("http://localhost:9000"), eqTo("42")).returns(Future(Some(extractorInfo.get)))

          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(POST, "/clusters/myCluster/projects/myProject/bindings").withJsonBody(Json.parse(
              """
                |{
                |"projectName":"myProject",
                |"bindings": [
                | {
                |     "queue": "myQueue",
                |     "tool": "myTool",
                |     "toolId": "42",
                |     "bindingType": "All Messages",
                |     "origBindingType": "None"
                |  }
                |]
                |}
              """.stripMargin
            )).withAuthenticator[DefaultEnv](identity.loginInfo)))
          val result: Result = Await.result(resultFut, 5 seconds)

          result.header.status must be equalTo(303)
          result.header.headers.get("Location").getOrElse("") must be equalTo("/?cluster=myCluster")

          there was one(mockRabbitAdminService).rebindProjectToQueue(
            eqTo("http://user:pass@host:10000"),
            eqTo("clowder-dev"),
            eqTo(project),
            eqTo("myQueue"),
            eqTo(extractorInfo.get.process),
            eqTo(BindingType.AllMessages))

        }
      }
    }
  }


  "Given I create invalid bindings" >> {
    "When I click submit" >> {
      "Then I receive an error" >> new Context {
        new WithApplication(application) {
          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(POST, "/clusters/dev/projects/myProject/bindings").withJsonBody(Json.parse(
              """
                |{
                |}
              """.stripMargin
            )).withAuthenticator[DefaultEnv](identity.loginInfo)))
          val result: Result = Await.result(resultFut, 5 seconds)

          result.header.status must be equalTo(BAD_REQUEST)
        }
      }
    }
  }

  "Given I provide an invalid clusterName" >> {
    "When I click submit" >> {
      "Then I receive an error" >> new Context {
        new WithApplication(application) {
          val project = new Project("myProject", ToolType.Extractor, "myCluster")
          mockProjectDAO.getProjectByName("badCluster", "myProject").returns(Future(Some(project)))

          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(POST, "/clusters/badCluster/projects/myProject/bindings").withJsonBody(Json.parse(
              """
                |{
                |"projectName":"myProject",
                |"bindings": [
                | {
                |     "queue": "myQueue",
                |     "tool": "myTool",
                |     "toolId": "42",
                |     "bindingType": "All Messages",
                |     "origBindingType": "None"
                |  }
                |]
                |}
              """.stripMargin
            )).withAuthenticator[DefaultEnv](identity.loginInfo)))
          val result: Result = Await.result(resultFut, 5 seconds)

          result.header.status must be equalTo(BAD_REQUEST)
        }
      }
    }
  }

  "Given I provide an invalid projectName" >> {
    "When I click submit" >> {
      "Then I receive an error" >> new Context {
        new WithApplication(application) {
          val project = new Project("myProject", ToolType.Extractor, "myCluster")
          mockProjectDAO.getProjectByName("myCluster", "badProject").returns(Future(None))

          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(POST, "/clusters/myCluster/projects/badProject/bindings").withJsonBody(Json.parse(
              """
                |{
                |"projectName":"myProject",
                |"bindings": [
                | {
                |     "queue": "myQueue",
                |     "tool": "myTool",
                |     "toolId": "42",
                |     "bindingType": "All Messages",
                |     "origBindingType": "None"
                |  }
                |]
                |}
              """.stripMargin
            )).withAuthenticator[DefaultEnv](identity.loginInfo)))
          val result: Result = Await.result(resultFut, 5 seconds)

          result.header.status must be equalTo(BAD_REQUEST)
        }
      }
    }
  }


  "Given I provide an a tool that doesn't exist" >> {
    "When I click submit" >> {
      "Then I receive an error" >> new Context {
        new WithApplication(application) {
          val project = new Project("myProject", ToolType.Extractor, "myCluster")
          mockProjectDAO.getProjectByName("myCluster", "myProject").returns(Future(Some(project)))
          mockToolsCatalogService.getExtractorInfo(any[WSClient],eqTo("http://localhost:9000"), eqTo("42")).returns(Future(None))

          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(POST, "/clusters/myCluster/projects/myProject/bindings").withJsonBody(Json.parse(
              """
                |{
                |"projectName":"myProject",
                |"bindings": [
                | {
                |     "queue": "myQueue",
                |     "tool": "myTool",
                |     "toolId": "42",
                |     "bindingType": "All Messages",
                |     "origBindingType": "None"
                |  }
                |]
                |}
              """.stripMargin
            )).withAuthenticator[DefaultEnv](identity.loginInfo)))
          val result: Result = Await.result(resultFut, 5 seconds)

          result.header.status must be equalTo(BAD_REQUEST)
        }
      }
    }
  }

  /**
    * The context.
    */
  trait Context extends Scope {
    val mockClusterStatsService = mock[ClusterstatsService]

    /**
      * A fake Guice module.
      */
    class FakeModule(rabbit: RabbitAdminService) extends AbstractModule with ScalaModule {
      val mockDB = mock[DefaultDB]

      def configure() = {
        bind[Environment[DefaultEnv]].toInstance(env)
        bind[DefaultDB].toInstance(mockDB)
        bind[ProjectDAO].toInstance(mockProjectDAO)
        bind[ClusterstatsService].toInstance(mockClusterStatsService)
        bind[RabbitAdminService].toInstance(rabbit)
        bind[ToolsCatalogService].toInstance(mockToolsCatalogService)
      }
    }

    /**
      * An identity.
      */
    val identity = User(
      userID = UUID.randomUUID(),
      loginInfo = LoginInfo("facebook", "user@facebook.com"),
      firstName = None,
      lastName = None,
      fullName = None,
      email = None,
      avatarURL = None,
      activated = true
    )

    /**
      * A Silhouette fake environment.
      */
    implicit val env: Environment[DefaultEnv] = new FakeEnvironment[DefaultEnv](Seq(identity.loginInfo -> identity))


    // Create my own cluster config
    val clusterMap = new java.util.HashMap[String, Object]()
    clusterMap.put("name", "myCluster")
    clusterMap.put("swarmURI", "http://user:pass@localhost:8888")
    clusterMap.put("vhost", "clowder-dev")

    val rabbitConfig = new util.ArrayList[Object]()
    val extractorConfig = new util.HashMap[String, Object]()

    extractorConfig.put("toolType", "Extractor")
    extractorConfig.put("rabbitMQURI", "http://user:pass@host:10000")
    extractorConfig.put("secret", "shh")

    rabbitConfig.add(extractorConfig)
    clusterMap.put("rabbitMQ", rabbitConfig)

    val clusterConfigList = new java.util.ArrayList[java.util.Map[String, Object]]()
    clusterConfigList.add(clusterMap)

    def app(rabbit: RabbitAdminService): Application = {
      new GuiceApplicationBuilder()
        .overrides(new FakeModule(rabbit))
        .configure(("clusterman.clusterConfigs", clusterConfigList), ("toolscatalog.uri", "http://localhost:9000"))
        .build()
    }

    lazy val application = app(mock[RabbitAdminService])
  }
}
